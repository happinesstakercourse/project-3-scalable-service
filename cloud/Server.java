// Server.java
// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/*
 * Main Entry Point for VM instance work.
 *
 * Cloud library would boot up VM and pass control to main procedure of
 * this file as Server instance.
 *
 * Server main procedure would setup connection to Admin server VM and enter
 * main worker loop according to the role assigned to this server.
 */

import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    private static AdminVMIntf admin = null;
    private static Cloud.DatabaseOps cache = null;

	public static void main ( String args[] ) throws Exception {
		if (args.length != 3)
		    throw new Exception("Need 3 args: <cloud_ip> <cloud_port> <VM id>");

		int thisPort = Integer.parseInt(args[1]);
		int thisVMID = Integer.parseInt(args[2]);

		/* RMI Connection Phase */

        Registry reg = LocateRegistry.getRegistry(args[0], thisPort);
        int role;
        AdminVM realAdmin;

        // Master Node create Admin RMI Instance
        if (thisVMID == 1) {

            // Create Admin Instance
            realAdmin = new AdminVM(args[0], thisPort);
            reg.rebind("Admin", realAdmin);
            role = AdminVM.ID_MASTER;

            // Boot cache server and 1 app server at initialization
            realAdmin.initializeCluster();

            admin = (AdminVMIntf) reg.lookup("Admin");
        }
        // All other nodes would seek master from RMI
        else {
            admin = (AdminVMIntf) reg.lookup("Admin");
            role = admin.getRole(thisVMID);
        }

        // Cache Node create Cache VM Instance
        if (role == AdminVM.ID_CACHE){
            CacheVM realCache = new CacheVM(args[0], thisPort);
            reg.rebind("Cache", realCache);
        }
        // Frontier and Middle Server would report booted to Admin
        // and setup connection with Cache Server
        else if (role == AdminVM.ID_MIDDLE || role == AdminVM.ID_FRONTIER){

            admin.reportMachineBooted(role);

            // Due to synchronization issue, some machine would get booted
            // before cache is running, then it needs to wait until cache
            // server is binded to RMI service.
            boolean cacheNotFound = true;
            while (cacheNotFound) {
                try {
                    cacheNotFound = false;
                    cache = (Cloud.DatabaseOps) reg.lookup("Cache");
                } catch (NotBoundException eNotBound) {
                    // Cache Server not up yet, wait 100 ms and try again
                    cacheNotFound = true;
                    Thread.sleep(100);
                }
            }
        }

        /* Configure main worker instance for this server */
        VM workingVM = null;
        if (role == AdminVM.ID_MIDDLE) {
            workingVM = new AppVM(args[0], thisPort,
                    thisVMID, role, admin, cache);
        } else if (role == AdminVM.ID_FRONTIER || role == AdminVM.ID_MASTER){
            workingVM = new FrontierVM(args[0], thisPort,
                    thisVMID, role, admin);
        }

        /* Start working! */
        if (role != AdminVM.ID_CACHE) {
            while (workingVM.serveNext());
            // Due to unknown reason, I must explicitly call end VM to
            // let this server give up control and terminate normally.
            workingVM.SL.endVM(thisVMID);
        }
	}
}

