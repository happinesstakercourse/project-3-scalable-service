// AdminVM.java
// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/*
 * Implementation of Admin VM
 *
 * In additional to management work, the most important work admin machine is
 * doing is to calculate and predict the load and update those statistics, so
 * that machines can be booted or shutdown accordingly.
 */


import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Deque;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedDeque;

public class AdminVM extends UnicastRemoteObject implements AdminVMIntf{

    // VM ID of the master machine
    public static final int masterID = 1;

    // definition of different roles
    public static final int ID_MASTER = 1;
    public static final int ID_FRONTIER = 2;
    public static final int ID_MIDDLE = 3;
    public static final int ID_CACHE = 4;

    // drop threshold
    public static final int PURCHASE_TIMEOUT_TH = 1500;
    public static final int BROWSE_TIMEOUT_TH = 700;

    // statistics of frontend machine
    private static int frontierBooting = 0;
    private static int exceedIndicator = 0;

    // statistics of app machine
    private static int middleBooting = 0;
    private static int middleRunning = 0;

    // data used to calculate and predict load
    private static int predictedLength = 0;
    private static long lastComputeTime = 0;
    private static int lastQueueLength = 0;
    private static double lastQueueChangeRate = 0.0;
    private static int droppingNumber = 0;

    // indicate whether cache/first middle server is up
    private static boolean systemUP = false;

    // role dictionary
    private static HashMap<Integer, Integer> VMRoleMap = new HashMap<>();

    // queues maintained by master
    private static Deque<RequestMeta> browseQueue =
            new ConcurrentLinkedDeque<>();
    private static Deque<RequestMeta> purchaseQueue =
            new ConcurrentLinkedDeque<>();

    private static ServerLib SL;


    public AdminVM(String ip, int port) throws RemoteException {
        SL = new ServerLib(ip, port);
        VMRoleMap.put(masterID, ID_MASTER);
    }

    @Override
    public int getRole(int VMID) throws RemoteException {
        if (VMID == masterID) {
            return ID_MASTER;
        }

        Integer role = VMRoleMap.get(VMID);
        if (role != null) {
            return role;
        } else {
            return -1;
        }
    }

    /**
     * Although initial design for this function is to determine how severe
     * overload in frontend server and boot several machines for it, I find
     * that in even really overloaded situation, frontend is not critical
     * bottleneck. So in most cases, this function would only be called to
     * boot one machine at a time.
     *
     * @param exceedLength Extent of overloading
     * @throws RemoteException Remote I/O exception
     */
    public synchronized void reportExceed(int exceedLength)
            throws RemoteException {
        // calculate machine to boot
        exceedIndicator += exceedLength;
        int machineNeedBooting = exceedIndicator - frontierBooting;

        // boot those machines
        if (machineNeedBooting > 0) {
            bootMachines(machineNeedBooting, ID_FRONTIER);
        }
    }

    @Override
    public synchronized void reportMachineBooted(int role)
            throws RemoteException {
        if (role == AdminVM.ID_FRONTIER) {
            frontierBooting -= 1;
            exceedIndicator -= 1;
        } else if (role == AdminVM.ID_MIDDLE){
            middleBooting -= 1;
            middleRunning += 1;
        }
    }

    @Override
    public synchronized boolean applyShutdown(int role, int id)
            throws RemoteException {
        // always permit frontier shutdown if valid
        if (role == AdminVM.ID_FRONTIER) {
            exceedIndicator -= 1;
            if (exceedIndicator < 0) {
                VMRoleMap.remove(id);
                exceedIndicator = 0;
                System.out.println("FRONT SHUTDOWN");
                return true;
            }
            return false;
        }

        // for app VM, check predicted queue length to decide
        if (role == AdminVM.ID_MIDDLE) {
            // No booting, no dropping => stable scale in
            if (middleBooting == 0 &&
                    droppingNumber == 0 &&
                    middleRunning >
                            ((purchaseQueue.size() + browseQueue.size()) / 2
                                    + 1)) {
                middleRunning -= 1;
                VMRoleMap.remove(id);
                System.out.println("APP SHUTDOWN");
                return true;
            }
        }

        return false;
    }

    @Override
    public void pushRequest(Cloud.FrontEndOps.Request r, int queueLength)
            throws RemoteException {
        // predict the time request is launched by cloud queue length
        if (r.isPurchase) {
            purchaseQueue.add(new RequestMeta(r,
                    System.currentTimeMillis() - 60 * queueLength));
        } else {
            browseQueue.add(new RequestMeta(r,
                    System.currentTimeMillis() - 60 * queueLength));
        }

        predictQueueLength();
    }

    @Override
    public synchronized RequestMeta getNextRequest() throws RemoteException {
        // prefer purchase operation because it makes money!
        long curTime = System.currentTimeMillis();
        if (!purchaseQueue.isEmpty()) {
            RequestMeta rm = purchaseQueue.poll();
            if (rm != null &&  curTime - rm.enqueueTime > PURCHASE_TIMEOUT_TH) {
                droppingNumber += 1;
            }
            return rm;
        } else {
            RequestMeta rm = browseQueue.poll();
            if (rm != null && curTime - rm.enqueueTime > BROWSE_TIMEOUT_TH) {
                droppingNumber += 1;
            }
            return rm;
        }
    }


    /**
     * Helper function to calculate and predict load
     *
     * My prediction on App server load is based on the glabal queue maintained
     * by master node.
     *
     * When system is cold (initial app server and cache server still booting),
     * the load prediction would use queue increasing speed, to estimate how
     * many servers would be capable of handling current load. (Because no
     * request can be handled now)
     *
     * When system is hot, the prediction would be based on current queue length
     * and the change rate of the queue. It is actually predicting the queue
     * length after 1 second, to boot or shutdown suitable amount of machines.
     */
    private synchronized void predictQueueLength() {

        int totalLength = purchaseQueue.size() + browseQueue.size();
        double queueChangeRate = 0.0;

        // first computation, just use current queue length
        if (lastComputeTime == 0) {
            lastComputeTime = System.currentTimeMillis();
            predictedLength = totalLength;
            lastQueueLength = totalLength;
        }
        // normal calculation
        else {
            long thisComputeTime = System.currentTimeMillis();

            // only calculate in frequency than one time per second
            // want to make calculation stable
            if (thisComputeTime - lastComputeTime > 1000) {

                // system hot, initial booted VMs running
                if (systemUP) {
                    // compensate dropped requests since last calculation.
                    // dropping would decrease queue very fast, this would
                    //   influence our calculation
                    queueChangeRate = (double)
                            (totalLength + droppingNumber - lastQueueLength) /
                                    (thisComputeTime - lastComputeTime);

                    // no prediction for shrinking
                    // because VMs can end immediately
                    if (queueChangeRate < 0) {
                        queueChangeRate = 0;
                    }

                    // average the queue change rate between two calculation
                    // to make prediction more stable
                    predictedLength = (int)
                            (totalLength +
                                    (queueChangeRate + lastQueueChangeRate)
                                            * 500);

                    lastComputeTime = thisComputeTime;
                    lastQueueLength = totalLength;
                    droppingNumber = 0;
                    lastQueueChangeRate = queueChangeRate;
                }
                // cold start, use queue increasing speed
                else {
                    queueChangeRate = (double)
                            (totalLength - lastQueueLength) /
                            (thisComputeTime - lastComputeTime);
                    predictedLength = (int)(queueChangeRate * 1000 + 3);
                    lastQueueLength = totalLength;
                    lastComputeTime = thisComputeTime;

                    // system already up now
                    if (SL.getStatusVM(2) ==
                            Cloud.CloudOps.VMStatus.Running) {
                        systemUP = true;
                    }
                }

                // queue length cannot be negative
                if (predictedLength < 0) {
                    predictedLength = 0;
                }

                // assume each App server can handle two requests in the queue
                // so that all requests would not timeout
                if (predictedLength / 2 > middleRunning + middleBooting) {
                    int machineToBoot =
                            (predictedLength / 2 -
                                    middleRunning - middleBooting);

                    // limit the number of machine to boot at one calculation
                    //   to handle sudden burst of traffic
                    if (machineToBoot > 3) {
                        machineToBoot = 3;
                    }
                    bootMachines(machineToBoot, ID_MIDDLE);
                }
            }
        }
    }


    /**
     * Helper function to boot initial servers
     *
     * This function is mainly used by Checkpoint1 according to time in the day.
     *
     * Now this function would only boot minimum initial server (1 app server
     * and 1 cache server)
     */
    public void initializeCluster() {
        bootMachines(1, ID_CACHE);
        bootMachines(1, ID_MIDDLE);
    }


    /**
     * Helper worker to boot VM machines
     *
     * @param amount Amount of machine to boot
     * @param role The role of booted machine
     */
    private void bootMachines(int amount, int role) {

        // use a loop to boot machine and save role
        for (int i=0; i<amount; i++) {
            int id = SL.startVM();
            VMRoleMap.put(id, role);
        }

        // update statistics
        if (role == AdminVM.ID_FRONTIER) {
            System.out.println("BOOT FRONT " + amount);
            frontierBooting += amount;
        } else if (role == AdminVM.ID_MIDDLE){
            System.out.println("BOOT APP " + amount);
            middleBooting += amount;
        }
    }
}
