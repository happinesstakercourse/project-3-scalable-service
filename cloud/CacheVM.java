// CacheVM.java
// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/*
 * Cache Server
 *
 * Our cache would save all browsing history in its cache, so next time the
 * same request for that item would get replied immediately to avoid the 50ms
 * delay in DB.
 *
 * DB write operation on that key would invalidate the cache entry.
 *
 * The cache uses a reader-writer lock to maintain concurrency correctness,
 * and provide low overhead on performance.
 *
 * Keys and values are trimmed as original Database implementation do, to
 * provide more cache hit due to space differences.
 */

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class CacheVM extends UnicastRemoteObject implements Cloud.DatabaseOps{

    private static Cloud.DatabaseOps defaultDB;
    private static ServerLib SL;
    private static ConcurrentHashMap<String, String> DBCache;

    private static final ReentrantReadWriteLock rwLock =
            new ReentrantReadWriteLock();

    public CacheVM(String ip, int port) throws RemoteException {
        SL = new ServerLib(ip, port);
        defaultDB = SL.getDB();
        DBCache = new ConcurrentHashMap<>();
    }

    @Override
    public String get(String key) throws RemoteException {
        key = key.trim();
        rwLock.readLock().lock();
        if (DBCache.containsKey(key)) {
            String val = DBCache.get(key);
            rwLock.readLock().unlock();
            return val;
        } else {
            rwLock.readLock().unlock();
            String val = defaultDB.get(key);
            rwLock.writeLock().lock();
            DBCache.put(key, val);
            rwLock.writeLock().unlock();
            return val;
        }
    }

    @Override
    public boolean set(String key, String val, String auth)
            throws RemoteException {
        rwLock.writeLock().lock();
        DBCache.put(key.trim(), val.trim());
        rwLock.writeLock().unlock();
        return defaultDB.set(key, val, auth);
    }

    @Override
    public boolean transaction(String item, float price, int qty)
            throws RemoteException {
        rwLock.writeLock().lock();
        DBCache.remove(item.trim());
        rwLock.writeLock().unlock();
        return defaultDB.transaction(item, price, qty);
    }
}
