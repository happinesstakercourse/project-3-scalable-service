// RequestMeta.java
// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/*
 * Helper class used to store Request information in master queue
 */


import java.io.Serializable;

public class RequestMeta implements Serializable{

    public Cloud.FrontEndOps.Request request;

    public long enqueueTime;

    public RequestMeta(Cloud.FrontEndOps.Request r, long t) {
        request = r;
        enqueueTime = t;
    }

}
