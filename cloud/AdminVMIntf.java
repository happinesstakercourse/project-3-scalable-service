// AdminVMIntf.java
// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/*
 * Admin VM Interface
 *
 * Admin VM is the master node of the whole system.
 *
 * It would maintain a global queue for all requests, collected from frontend
 * servers, and pulled by app servers.
 * It would also manage booting and shutdown of machines.
 */


import java.rmi.Remote;
import java.rmi.RemoteException;

public interface AdminVMIntf extends Remote{

    /**
     * Get the assigned role of corresponding VM ID. (frontier/middle/cache)
     *
     * @param VMID ID of query VM
     * @return Role of that VM, -1 on error
     * @throws RemoteException Remote I/O exception
     */
    int getRole(int VMID) throws RemoteException;

    /**
     * Booted machine report to master node right before running.
     * Master node can update statistics for number of machine booting or
     * running.
     *
     * @param role Role of the VM booted
     * @throws RemoteException Remote I/O exception
     */
    void reportMachineBooted(int role) throws RemoteException;

    /**
     * (Used by frontend servers)
     * Frontend server would report to master node if its queue is very long,
     * and master node would boot more frontend servers accordingly.
     *
     * @param exceedLength Extent of overloading
     * @throws RemoteException Remote I/O exception
     */
    void reportExceed(int exceedLength) throws RemoteException;

    /**
     * (Used by frontend/middle servers)
     * Servers would apply for shutdown if they feel the load is low. Master
     * node would check whether load is really going down and decide approve
     * the shutdown or not.
     *
     * @param role Role of the VM requesting shutdown
     * @param id ID of the VM requesting shutdown
     * @return Dicision on whether shutdown approved
     * @throws RemoteException Remote I/O exception
     */
    boolean applyShutdown(int role, int id) throws RemoteException;

    /**
     * Frontend server push the request they pulled from their cloud queue,
     * and push to the global queue maintained by master.
     *
     * @param r Request from client
     * @param queueLength Cloud queue length of that VM
     * @throws RemoteException Remote I/O exception
     */
    void pushRequest(Cloud.FrontEndOps.Request r, int queueLength)
            throws RemoteException;

    /**
     * App server pull request from the global queue maintained by master node.
     *
     * @return Request from client, with timestamp information
     * @throws RemoteException Remote I/O exception
     */
    RequestMeta getNextRequest() throws RemoteException;
}
