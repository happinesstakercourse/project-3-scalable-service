// VM.java
// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/*
 * Abstract base class for all VM instances.
 *
 * Base VM class contains information related to Cloud (IP, Port, ID)
 * and information assigned by master (role, admin)
 */

abstract public class VM {

    protected String cloudIP;
    protected int cloudPort;
    protected int VMID;
    protected AdminVMIntf admin;
    protected int role;
    protected ServerLib SL;

    public VM(String ip, int port, int id, int a_role, AdminVMIntf ad) {
        cloudIP = ip;
        cloudPort = port;
        VMID = id;
        admin = ad;
        role = a_role;
        SL = new ServerLib(ip, port);
    }

    /**
     * Main worker procedure to serve requests according to VM role
     *
     * @return if false, shutdown VM now
     */
    abstract public boolean serveNext();
}
