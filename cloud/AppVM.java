// AppVM.java
// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/*
 * Application VM (Middle Tier)
 *
 * App VM would pull request from the global queue maintained by master node
 * and send those requests to the cache server.
 *
 * Requests which have very high probability to timeout after processing would
 * get dropped directly by App VM to save resource and serve more clients.
 *
 * If App VM cannot get more requests, they would apply shutdown to master node,
 * and if approved, it would exit loop and shutdown machine.
 */


import java.rmi.RemoteException;

public class AppVM extends VM{

    private Cloud.DatabaseOps cache;

    public AppVM(String ip, int port, int id, int a_role,
                 AdminVMIntf ad, Cloud.DatabaseOps ca) {
        super(ip, port, id, a_role, ad);
        cache = ca;
    }

    @Override
    public boolean serveNext() {

        // get request from global queue
        RequestMeta reqMeta = null;
        try {
            reqMeta = admin.getNextRequest();
        } catch (RemoteException eRemote) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException eInt) {
                return true;
            }
            return true;
        }

        // apply shutdown because no more requests available
        // since there may be situation that suddenly many requests get dropped
        //   and some App VM would not get request, and in this situation we
        //   do not want machine to shutdown because the load is still high, I
        //   repeat this process for several times to make sure the load is
        //   really going down.
        int sleepCtr = 0;
        while (reqMeta == null) {
            try {
                sleepCtr++;
                if (sleepCtr > 4) {
                    return !admin.applyShutdown(role, VMID);
                }
                Thread.sleep(300);
                reqMeta = admin.getNextRequest();
            } catch (RemoteException eRemote) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException eInt) {
                    return true;
                }
                return true;
            } catch (InterruptedException eInt) {
                return true;
            }
        }

        if (reqMeta.request == null) {
            return true;
        }

        // purchase would have 200ms doBackEndWork and 50ms*2 DB delay
        // assume average 200ms frontend delay
        // we give 1500ms as upper limit for non-timeout requests
        if (reqMeta.request.isPurchase) {
            if (System.currentTimeMillis() - reqMeta.enqueueTime >
                    AdminVM.PURCHASE_TIMEOUT_TH) {
                SL.drop(reqMeta.request);
                return true;
            } else {
                SL.processRequest(reqMeta.request, cache);
            }
        }
        // browse would have 200ms doBackEndWork and 50ms DB delay
        // so 700ms is good estimation for timeout
        else {
            if (System.currentTimeMillis() - reqMeta.enqueueTime >
                    AdminVM.BROWSE_TIMEOUT_TH) {
                SL.drop(reqMeta.request);
                return true;
            } else {
                SL.processRequest(reqMeta.request, cache);
            }
        }

        return true;
    }

}
