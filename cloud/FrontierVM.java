// FrontierVM.java
// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/*
 * Frontend Server
 *
 * Frontend server would pull request from Cloud queue assigned to it using
 * SL.getNextRequest() (takes 60ms) if it is registered to the cloud.
 *
 * Then it would push the request to the global queue maintained by master VM.
 *
 * Since the processing time in this tier is really short, it would only become
 * performance bottleneck and require booting more frontier VM when requests
 * comes in interval ~100ms or less.
 *
 * Based on this observation, I make Frontend Server grow slowly (only grow
 * when queue is very long) and shrink relatively quickly (when no requests
 * in the queue).
 */


import java.rmi.RemoteException;

public class FrontierVM extends VM{

    public static final int QUEUE_UPPER_LIMIT = 3;
    public static final int QUEUE_LOWER_LIMIT = 0;

    private static boolean additionalFrontier = false;
    private float lastReportTime = 0;

    public FrontierVM(String ip, int port, int id, int a_role, AdminVMIntf ad) {
        super(ip, port, id, a_role, ad);
        SL.register_frontend();
    }

    @Override
    public boolean serveNext() {

        // each process (get request + push to deque) would take 60 ms
        // we can allocate 200ms for this task, thus queue length 3 is
        // upper limit


        // measure the time to get this request(indicate income traffic density)
        long start = System.currentTimeMillis();
        Cloud.FrontEndOps.Request r = SL.getNextRequest();
        long end = System.currentTimeMillis();

        int queueLength = SL.getQueueLength();
        try {
            admin.pushRequest(r, queueLength);

            // request more frontend machine if queue is long, and
            // last booted VM should be up and running
            if (queueLength > QUEUE_UPPER_LIMIT &&
                    System.currentTimeMillis() - lastReportTime > 5000) {
                admin.reportExceed( 1);
                lastReportTime = System.currentTimeMillis();
            }

            // shrink frontend VM if no request in queue and it takes
            // long time to get next request
            if (role != AdminVM.ID_MASTER &&
                    queueLength <= QUEUE_LOWER_LIMIT &&
                    (end - start) > 500) {
                if (admin.applyShutdown(role, VMID)) {
                    SL.unregister_frontend();
                    // process remaining requests
                    while ((queueLength = SL.getQueueLength()) > 0) {
                        r = SL.getNextRequest();
                        admin.pushRequest(r, queueLength);
                    }

                    return false;
                }
            }
        } catch (RemoteException eRemote) {
            System.out.println("Cannot contact admin!");
        }

        // drop head of queue if queue is already very long, cannot serve
        // them before timeout, do not waste middle tier resource
        while (SL.getQueueLength() > QUEUE_UPPER_LIMIT + 1) {
            SL.dropHead();
        }

        return true;
    }

}
